package com.ivyft.hive.hadoop;

import com.google.protobuf.GeneratedMessage;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.mapred.*;

import java.io.IOException;

/**
 * <pre>
 *
 * Created by IntelliJ IDEA.
 * User: zhenqin
 * Date: 15/4/8
 * Time: 14:40
 * To change this template use File | Settings | File Templates.
 *
 * </pre>
 *
 * @author zhenqin
 */
public class IntLengthHeaderMr1InputFormat extends FileInputFormat<LongWritable, BytesWritable> {



    public static void setInputObjectClass(Configuration conf, Class<? extends GeneratedMessage> clazz) {
        conf.set(IntLengthHeaderInputFormat.PROTOBUF_CLASS, clazz.getName());
    }


    @Override
    protected boolean isSplitable(FileSystem fs, Path filename) {
        return false;
    }

    @Override
    public RecordReader<LongWritable, BytesWritable> getRecordReader(InputSplit split, JobConf job, Reporter reporter) throws IOException {
        if(split instanceof FileSplit) {
            return new IntLengthHeaderMr1RecordReader((FileSplit)split, job);
        }
        throw new IllegalArgumentException();
    }
}
